import numpy as np
from numpy.polynomial import polynomial as pl


# bxidx = 3
# bxdelivered = 2
# livefrac = 0.000000003323
# bxlumi = np.array([1, 2, 3, 4, 5])
# thispresc = 4


def bxlumi_poly1d(bxlumi, bxmask, poly1dindex):
    p1 = np.poly1d(poly1dindex)
    print(p1)
    if bxmask.shape[0] > 0:
        bxlumi_prime = np.ma.array(bxlumi, mask=bxmask, fill_value=0, dtype=float)
    else:
        bxlumi_prime = bxlumi

    corrected_bxlumi = p1(bxlumi_prime)
    return corrected_bxlumi.filled()


def bxlumi_leading_poly1d(bxlumi, bxmask, poly1dindex_leading, poly1dindex_train):
    p1_leading = np.poly1d(poly1dindex_leading)
    p1_train = np.poly1d(poly1dindex_train)
    print(p1_leading, p1_train)
    # resolve leading bunch and train bunches
    leading_mask = (np.logical_xor(bxmask, np.roll(bxmask, 1)) & bxmask)
    bx_leading = np.ma.array(bxlumi, mask=leading_mask, fill_value=0, dtype=float)
    train_mask = np.logical_xor(leading_mask, bxmask)
    bx_train = np.ma.array(bxlumi, mask=train_mask, fill_value=0, dtype=float)
    corrected_bxlumi = [p1_leading(bx_leading).filled(), p1_train(bx_train).filled()]
    return corrected_bxlumi


def bx_mod(bxlumi, pattern):
    """Modify the order, without reminder """
    d = bxlumi.__len__() // pattern.__len__()  #distance
    r = bxlumi.__len__() % pattern.__len__()  #reminder
    if r == 0:
        bxlumi_mod = bxlumi.reshape(d, pattern.__len__())
        bxlumi_mod = bxlumi_mod[:, pattern]
    else:
        rs = bxlumi.__len__() - r
        bxlumi_mod = bxlumi[0:rs]  # last 2 elemnts removed
        bxlumi_mod = bxlumi_mod.reshape(d, pattern.__len__())
        bxlumi_mod = bxlumi_mod[:, pattern]
        bxlumi_mod = np.append(bxlumi_mod, bxlumi[-r:])  # add 2 last elements

    return bxlumi_mod.flatten()


def bxlumi_mod4_poly1d(bxlumi, bxmask, poly1dindex_mod4, poly1dindex_rest):
    p1_mod4 = np.poly1d(poly1dindex_mod4)
    p1_rest = np.poly1d(poly1dindex_rest)
    '''Anne:BX(Mod(4)) this can equal 1, 2, 3, 0
     First one or all? Doing all for now(here)
     '''
    bxlumi_mod = bx_mod(bxlumi, [1, 2, 3, 0])
    if bxmask.shape[0] > 0:
        bxlumi_mod = np.ma.array(bxlumi_mod, mask=bxmask, fill_value=0, dtype=float)
    else:
        bxlumi_mod = bxlumi_mod

    return p1_mod4(bxlumi_mod)


def bxlumi_mod4_poly1d_f1(bxlumi, bxmask, poly1dindex_mod4, poly1dindex_rest):
    p1_mod4 = np.poly1d(poly1dindex_mod4)
    p1_rest = np.poly1d(poly1dindex_rest)
    '''
        Doing one and rest(here)
     '''
    p1_bxlumi_mod = bx_mod(bxlumi, [1, 2, 3, 0])
    rest_bxlumi_mod = bx_mod(bxlumi, [1, 2, 3, 0])
    if bxmask.shape[0] > 0:
        p1_bxlumi_mod = np.ma.array(p1_bxlumi_mod, mask=bxmask, fill_value=0, dtype=float)
        rest_bxlumi_mod = np.ma.array(rest_bxlumi_mod, mask=bxmask, fill_value=0, dtype=float)
    else:
        bxlumi_mod = p1_bxlumi_mod

    return p1_mod4(bxlumi_mod)


def bxlumi_poly2d(bxlumi, bxmask, poly2dindex):
    bxlumi_prime = np.ma.array(bxlumi, mask=bxmask, fill_value=0, dtype=float)
    # get total lumi by summing colliding bunches
    total_lumi = np.sum(bxlumi_prime)
    total_lumi = np.full_like(bxlumi_prime, total_lumi)
    # apply poly2d function on each bunch
    corrected_bxlumi = pl.polyval2d(bxlumi_prime, total_lumi, poly2dindex)
    print(corrected_bxlumi)
    return corrected_bxlumi


if __name__ == '__main__':
    print("Poly1d")
