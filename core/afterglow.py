import numpy as np

# bxidx = 3
# bxdelivered = 2
# livefrac = 0.000000003323
# bxlumi = np.array([1, 2, 3, 4, 5])
# thispresc = 4


def bxlumi_afterglow_threshold(bxlumi, bxmask, thresholds):
    if bxmask.shape[0] > 0:
        collidingbx = np.ma.array(bxlumi, mask=bxmask, fill_value=0, dtype=float)
    else:
        collidingbx = bxlumi

    for (bxthreshold, c) in thresholds:
        print(bxthreshold)
        if collidingbx >= bxthreshold:
            afterglow = c

    print(afterglow)
    return 0


if __name__ == '__main__':
    print("Afterglow")
