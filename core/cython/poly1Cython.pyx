import numpy as np
cimport numpy as np
from cpython cimport array
import array

#cdef double bxidx = 3
#cdef double bxdelivered = 2
#cdef double livefrac = 0.000000003323
#cdef double [:] bxlumi = np.transpose(np.array([bxidx + 1, bxdelivered, bxdelivered * livefrac]))


cdef bxlumi_poly1d(double[:] bxlumi, double[:]  bxmask, double[:] poly1dindex) :
    p1 = np.poly1d(poly1dindex)
    print(p1)
    cdef Py_ssize_t lmask = bxmask.shape[0]
    if lmask > 0:
        bxlumi_prime = np.ma.array(bxlumi, mask=bxmask, fill_value=0, dtype=float)
    else:
        bxlumi_prime = bxlumi

    corrected_bxlumi = p1(bxlumi_prime)
    return corrected_bxlumi.filled()



if __name__ == '__main__':
    print("Poly1d")
