import core.poly1d as poly1d
import core.afterglow as afg
import numpy as np
from core import __all__


class Plugin:
    """Base class for all plugins."""
    plugins = []

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        cls.plugins.append(cls())  #all of subclases are included


    # def load_plugin(self, plugin_name):
    #     """ Loads a single plugin given its name """
    # # Load the plugin only if not loaded yet
    #     module = __import__("core." + plugin_name, fromlist=["plugins"])
    #     plugin = module.load()
    #     self.plugins[plugin_name] = plugin
    #     return plugin


class Poly1d(Plugin):
    title = 'Poly1d plugin for functions'
    view = 'task_detail'

    def __init__(self, bxlumi=None, bxmask=None):
        print("Poly1d module instance created")
        self.bxlumi = bxlumi
        self.bxmask = bxmask

    def bxlumi_poly1d(self, poly1dindex):
        return poly1d.bxlumi_poly1d(self.bxlumi, self.bxmask, poly1dindex)

    def bxlumi_leading_poly1d(self, poly1dindex_leading, poly1dindex_train):
        return poly1d.bxlumi_leading_poly1d(self.bxlumi, self.bxmask, poly1dindex_leading, poly1dindex_train)

    def bxlumi_mod4_poly1d(self, poly1dindex_mod4, poly1dindex_rest):
        return poly1d.bxlumi_mod4_poly1d(self.bxlumi, self.bxmask, poly1dindex_mod4, poly1dindex_rest)

    def bxlumi_poly2d(self, poly2dindex):
        return poly1d.bxlumi_poly2d(self.bxlumi, self.bxmask, poly2dindex)


class AfterGlow(Plugin):
    title = 'AfterGlow plugin for functions'
    view = 'task_detail'

    def __init__(self, bxlumi=None, bxmask=None):
        print("AfterGlow module instance created")
        self.bxlumi = bxlumi
        self.bxmask = bxmask

    def bxlumi_threshold(self, thresholds):
        return afg.bxlumi_afterglow_threshold(self.bxlumi, self.bxmask, thresholds)

# for plugin in Plugin.plugins:
#     plugin.do_work()
