import tables as t, pylab
import os, sys,  yaml, csv


def read_yaml_file(file):
    with open(file) as fh:
        read_data = list(yaml.safe_load_all(fh))
        return read_data


def read_all_csv_fills(file, missing_fills, n_channels, shift):  # Read all fills in csv file
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader) # skip first file  has it contains the titles of the columns (check it on the .csv input file)
        fills_effs = {}
        if missing_fills:
            first_fill = next(csv_reader)
            fills_effs[int(first_fill[0])] = get_effs(first_fill, n_channels, shift)
            for mf in missing_fills:        #For missing fills, there are equal to the first fill
                fills_effs[int(mf)] = fills_effs[int(first_fill[0])]
        for row in csv_reader:
            fills_effs[int(row[0])] = get_effs(row, n_channels, shift)
    return fills_effs


def get_effs(fill, n_channels, shift):
    effs = []
    for i in range(n_channels):
        effs.append(fill[i + shift])#Note that this assumes you have a column in the csv files for all 16 PLT channels, also those that are disable (have -99 in the column value)
    return effs



def read_csv_fills(file, fills, n_channels, shift): # Read only a number of fills in csv file
    with open(file) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        next(csv_reader) # skip first file  has it contains the titles of the columns (check it on the .csv input file)
        fills_effs = {}
        for row in csv_reader:
            if int(row[0]) in fills:
                fills_effs[int(row[0])] = get_effs(row, n_channels, shift)
    return fills_effs


def read_bril_files(input_path, fills, years):
    files = {}
    for year in years:
        files_fills = {}
        for fill in fills:
            dir = input_path+'/'+str(year)+'/'+str(fill)+'/'
            if os.path.exists(dir):
                files_fills[fill] = os.listdir(input_path+'/'+str(year)+'/'+str(fill)+'/')
        files[year] = files_fills
    return files

def open_hd5(input_path, year, fill, file):
    in_file = input_path+year+'/'+fill+'/'+file
    h5in = t.open_file(in_file, mode='r')
    return h5in


def close_hd5(hd5):
    hd5.close()


def read_hd5_table(hd5, table_name):
    table = []
    if(table_name not in hd5):
        # print("Fill "+fill+"("+year+") File "+file+" has a problem (table:"+table_name+" not in input file)")
        print("Hd5 file has a problem (Table:"+table_name+" not in input file))")
        close_hd5(hd5)
    else:
        table = hd5.get_node(table_name)
    return table


def write_hd5_table(output_path, file, table):
   hd5 = t.open_file(output_path+file,mode='w')
   hd5.create_table('/', table.name, table.description, filters=table.filters,chunkshape=table.chunkshape)
   hd5.close()


