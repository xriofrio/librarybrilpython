import core.file_manager as fm
import numpy as np

class DefaultValues:
    """Default values for BRIL"""
    default = []
    default_file = 'default_values.yml'
    numBunchesLHC = int
    nchannels = int
    sigvis = float
    k = float
    init_n = int
    entries = int
    shifts = int  #shifts left csv file
    input_path = str
    sbil_ch = None
    sbil_ch_w = None
    lumi_orbit_ch = None
    lumi_orbit_ch_w = None



    def __init__(self, default_file = None):
        print("Default values instance created")
        if default_file == None:
            yaml_default = fm.read_yaml_file(self.default_file)[0]
        else:
            yaml_default = fm.read_yaml_file(default_file)[0]
        self.numBunchesLHC = yaml_default['numBunchesLHC']
        self.nchannels = yaml_default['nchannels']
        self.sigvis = yaml_default['sigvis']
        self.k = 11246. / self.sigvis
        self.init_n = yaml_default['init_n']
        self.entries = yaml_default['entries']
        self.shifts = yaml_default['shifts']
        self.input_path = yaml_default['input_path']
        self.sbil_ch = np.zeros((self.nchannels,self.numBunchesLHC))
        self.sbil_ch_w = np.zeros((self.nchannels, self.numBunchesLHC))
        self.lumi_orbit_ch = [[] for i in range(self.nchannels)]
        self.lumi_orbit_ch_w = [[] for i in range(self.nchannels)]

