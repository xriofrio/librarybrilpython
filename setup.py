# coding: utf-8
import os
import sysconfig
import numpy
from setuptools import setup, find_packages
from setuptools.command.build_py import build_py as _build_py

from Cython.Build import cythonize


excluded = [
    'core/corrector.py',
    'tests/testPlugins.py'
]


# noinspection PyPep8Naming
class build_py(_build_py):

    def find_package_modules(self, package, package_dir):
        ext_suffix = sysconfig.get_config_var('EXT_SUFFIX')
        modules = super().find_package_modules(package, package_dir)
        filtered_modules = []
        for (pkg, mod, filepath) in modules:
            if os.path.exists(filepath.replace('.py', ext_suffix)):
                continue
            filtered_modules.append((pkg, mod, filepath, ))
        return filtered_modules


def get_ext_paths(root_dir, excluded):
    """get filepaths for compilation"""
    paths = []

    for root, dirs, files in os.walk(root_dir):
        for filename in files:
            if os.path.splitext(filename)[1] != '.pyx':
                continue

            file_path = os.path.join(root, filename)
            if file_path in excluded:
                continue

            paths.append(file_path)


    return paths


setup(
    name='bril_lib',
    version='0.1.1',
    packages=['tests', 'core'],
    excluded=excluded,
    # packages=find_packages(),
    ext_modules=cythonize(
        get_ext_paths('core', excluded),
        compiler_directives={'language_level': 3},
        annotate=True
    ),
    include_dirs=[numpy.get_include()],
    cmdclass={
        'build_py': build_py
    }
)