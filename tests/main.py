from core.cython import poly1Cython as poly1
#import poly1ex as poly1dPython
import numpy as np
import argparse

def poly1d(bxlumi):
	print(".apply_poly1d(bxlumi,params)")
	a = np.random.rand(2)*7
	params = np.random.rand(10)*8
	print(poly1.apply_poly1d(bxlumi,params))


def after_glow():
	print('After glow')


if __name__ == '__main__':
	# parser = argparse.ArgumentParser(
	# 	prog='Bril lib',
	# 	description='Brilcalc Library for corrections and more')
	# parser.add_argument('--poly1d', metavar='p1', required=True,
	# 					help='Polynomial in degree N ',dest='p1')
	# parser.add_argument('--after_glow', metavar='afg', required=False,
	# 					help='after_glow in degree N ',dest='afg')
	# parser.add_argument('integers', metavar='N', type=float, nargs='+',
	# 					help = 'an float for the polynomial')
	# parser.add_argument('--ex', metavar='ex', required=False, default=32,
	# 					help='example schema')
    #
	# args = parser.parse_args()
	# N = args.integers
	# #main(workspace=args.workspace, schema=args.schema, dem=args.dem)
	# print(args)
	poly1d([1,2,3])



