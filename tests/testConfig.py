import numpy as np
import core.file_manager as fm
import time

if __name__ == '__main__':
    file = 'config.yml'
    yaml_data = fm.read_yaml_file(file)
    print(yaml_data[0])
    # all_fills_effs = fm.read_all_csv_fills(yaml_data[0]['csv']['input_channels'], yaml_data[0]['csv']['missing_fills'], yaml_data[0]['csv']['n_channels'], yaml_data[0]['csv']['shift'])
    # print(fills_effs)
    # fills_effs = fm.read_csv_fills(yaml_data[0]['csv']['input_channels'], yaml_data[0]['fills'], yaml_data[0]['csv']['n_channels'], yaml_data[0]['csv']['shift'])
    # # print(fills_effs)
    input = yaml_data[0]['input_path']
    files = fm.read_bril_files(input, yaml_data[0]['fills'], yaml_data[0]['years'])
    # print(files)
    hd5_in=fm.open_hd5(input, '17' ,'6191' ,'6191_302627_1709112251_1709120107.hd5')
    bxmask = fm.read_hd5_table(hd5_in, '/beam')[0]['collidable']
    print(bxmask)
    table = fm.read_hd5_table(hd5_in, yaml_data[0]['out_table_name'][0])
    print(table)
    fm.close_hd5(hd5_in)

    start_time = time.time()
    yaml_data = fm.read_yaml_file(file)
    print(yaml_data[0])
    print("The execution time is %s seconds ---" % (time.time() - start_time))

    #print(fill)


