import numpy as np
import core.plugin_manager as pg

b_arr = [True, False]
bxmask = np.random.choice(b_arr, size=16)
bxlumi = np.array(np.random.rand(16) * 8, dtype=float)
# bxlumi = np.arange(20)
p1IndexExample = [4, 3, 2, 1, 6]
p2IndexExample = [1.2, 2.3, 3.2, 4.1, 5.6]
afgExample = [(9, 1), (2, 3)]

if __name__ == '__main__':
    print(bxlumi)
    # print("Test plugins:")
    # plugins = pg.Plugin.plugins
    # print(plugins)
    # print("Test poly1d:")
    # Poly1d = pg.Poly1d(bxlumi, bxmask)
    # print(Poly1d.bxlumi_poly1d(p1IndexExample))
    # print(Poly1d.bxlumi_leading_poly1d(p1IndexExample, p1IndexExample))
    # print(Poly1d.bxlumi_mod4_poly1d(p1IndexExample, p2IndexExample))
    print("Test poly2d:")
    AG = pg.AfterGlow(bxlumi, bxmask)
    AG.bxlumi_threshold(afgExample)

