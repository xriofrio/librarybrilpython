import math

import numpy as np, pylab as py
import core.file_manager as fm
import time
import core.default_values as dv

if __name__ == '__main__':
    file = 'config.yml'
    start_time = time.time()
    fill = '6191'
    dfv = dv.DefaultValues()
    yaml_data = fm.read_yaml_file(file)
    print(yaml_data[0])
    #read fills
    fills_effs = fm.read_all_csv_fills(yaml_data[0]['csv']['input_channels'], yaml_data[0]['csv']['missing_fills'],
                                           dfv.nchannels, dfv.shifts)
    print(fills_effs[6191])
    #read files
    input = yaml_data[0]['input_path']
    files = fm.read_bril_files(input, yaml_data[0]['fills'], yaml_data[0]['years'])
    print(files)

    print("The fill number is  6191" )
    hd5_in=fm.open_hd5(input, '17' ,fill ,'6191_302627_1709112251_1709120107.hd5')
    bxmask = fm.read_hd5_table(hd5_in, '/beam')[0]['collidable']
    #print(bxmask)
    table = fm.read_hd5_table(hd5_in, yaml_data[0]['table_name'])
    print(table)
    cont=0


    slope_L = [-99, 0.0166, 0.0137, -99, -99, 0.0148, 0.0164, 0.0163, 0.0127, 0.0142, 0.0168, 0.0182, 0.0166, 0.0136,
               0.0118, 0.0116]
    slope_T = [-99, 0.0105, 0.0122, -99, -99, 0.0093, 0.0108, 0.0029, 0.0038, 0.0005, -0.0007, 0.0142, 0.0026, 0.0153,
               0.0096, 0.0009]  # coefficients
    leading = (py.logical_xor(bxmask, py.roll(bxmask, 1)) & bxmask)
    train = py.logical_xor(leading, bxmask)
    for row in table.iterrows():
        cont=cont+1
        if cont%2000==0:
            print(row)
        rawDataFixed = row['data']
        numorbits = math.ceil(float(max(rawDataFixed)) / 1024.) * 1024.
        # Default measurements
        dfv.sbil_ch[int(row['channelid'])] = -1 * dfv.k * np.log([float(x) / numorbits for x in rawDataFixed])
        lumi_orbit_ch_ = sum(dfv.sbil_ch[int(row['channelid'])])
        dfv.lumi_orbit_ch[int(row['channelid'])].append(lumi_orbit_ch_)
    # Weighted measurements
        mu = -1 * dfv.k * np.log([float(x) / numorbits for x in rawDataFixed])

        eff = leading / float(fills_effs[int(fill)][int(row['channelid'])]) + train / float(fills_effs[int(fill)][int(row['channelid'])])
        slope = leading * slope_L[int(row['channelid'])] / 100 + train * slope_T[int(row['channelid'])] / 100
        mu = mu * eff  ## aqui hace el cambio
        mu = mu - mu * mu * slope
        dfv.sbil_ch_w[int(row['channelid'])] = mu
        lumi_orbit_ch_w_ = sum(dfv.sbil_ch_w[int(row['channelid'])])
        dfv.lumi_orbit_ch_w[int(row['channelid'])].append(lumi_orbit_ch_w_)

    fm.close_hd5(hd5_in)
    print("The execution time is %s seconds ---" % (time.time() - start_time))

    #print(fill)


