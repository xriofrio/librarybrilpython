# LibraryBrilPython

### Goal
   a function library where BRIL specific algorithms are generalized and implemented with externalized parameters.    

### work flow
   1. collect use cases 
   2. implement use cases as python functions 
   3. design the python function in python
